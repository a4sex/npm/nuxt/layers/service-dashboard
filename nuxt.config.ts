import { createResolver } from '@nuxt/kit'

const { resolve } = createResolver(import.meta.url)

export default defineNuxtConfig({
  telemetry: false,
  components: [
    { path: resolve('components/global'), global: true, prefix: '' },
    { path: resolve('components/templates'), global: true, prefix: '' },
    { path: resolve('components/elements'), global: true, prefix: '' },
    { path: resolve('components/dialogs'), global: true, prefix: '' },
    { path: resolve('components/forms'), global: true, prefix: '' }
  ],

})